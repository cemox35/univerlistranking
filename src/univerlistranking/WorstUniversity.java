/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */
 
public class WorstUniversity extends University {

	public WorstUniversity(String name, int id, UrapPoints urapScores, double entrepreneurship, int prof, int docent,
			int docentYard, int instructor, int foreignStudent, int foreignInstructor, int disabledStudent,
			ExchangeStudent exchangeStudents, BoomSocial boomsocial, StudentNumbers students) {
		super(name, id, urapScores, entrepreneurship, prof, docent, docentYard, instructor, foreignStudent, foreignInstructor,
				disabledStudent, exchangeStudents, boomsocial, students);
		// TODO Auto-generated constructor stub
	}
	
	public WorstUniversity(String name, int id) {
		super(name, id);
		// TODO Auto-generated constructor stub
	}
	
	public WorstUniversity() {
		super("Worst University",999,new UrapPoints(999, 999, 999, 999, 999),999,999,999,999,999,999,999,999,new ExchangeStudent(999, 999, 999, 999, 999, 999, 999),new BoomSocial(999, 999, 999, 999, 999, 999, 999, 999, 999),new StudentNumbers(999, 999, 999, 999, 999, 999, 999, 999, 999, 999));
		technopark=0;
		incubationCenter=0;
	}


	public void worstBoomsocial(int int1, int int2, int int3, int int4, int int5, int int6, int int7, int int8,
			int int9) {
		boomsocial.worstBoomSocial(int1, int2, int3, int4, int5, int6, int7, int8, int9);
	}

	public void worstDisabledStudent(int int1) {
		if(disabledStudent > int1 && int1 !=0)
			disabledStudent=int1;
	}

	public void worstDocent(int int1) {
		if(docent > int1 && int1 !=0)
			docent=int1;
	}

	public void worstDocentYard(int int1) {
		if(docentYard > int1 && int1 !=0)
			docentYard=int1;
	}

	public void worstEntrepreneurship(double double1) {
		if(entrepreneurship > double1 && double1 !=0)
			entrepreneurship=double1;
	}

	public void worstForeignInstructor(int int1) {
		if(foreignInstructor > int1 && int1 !=0)
			foreignInstructor=int1;
		
	}

	public void worstForeignStudent(int int1) {
		if(foreignStudent > int1 && int1 !=0)
			foreignStudent=int1;
		
	}

	public void worstInstructor(int int1) {
		if(instructor > int1 && int1 !=0)
			instructor=int1;
		
	}

	public void worstProf(int int1) {
		if(prof > int1 && int1 !=0)
			prof=int1;
		
	}
	
	public void worstExchangeStudents(int int1, int int2, int int3, int int4, int int5, int int6, int int7) {
		exchangeStudents.worstExchangeStudent(int1, int2, int3, int4, int5, int6, int7);
	}
	
	public void worstStudents(int int1, int int2, int int3, int int4, int int5, int int6, int int7, int int8, int int9,
			int int10) {
		students.worstStudentNumbers(int1, int2, int3, int4, int5, int6, int7, int8, int9, int10);
	}

	public void worstUrapScores(double double1, double double2, double double3, double double4, double double5) {
		urapScores.worstUrapPoints(double1, double2, double3, double4, double5);
	}

	public void worstFeatures(int int1) {
		if (campusOpportunities>int1 && int1 != 0) {
			campusOpportunities=int1;
		}
		
	}

	public void worstFavorite(int int1) {
		if (favorites > int1 && int1 !=0) {
			favorites=int1;
		}
		
	}

	public void worstPlacementRatio(double double1) {
		if (placementRatio > double1 && double1 != 0) {
			// NO NEED FOR CHECK IF DOUBLE 1 IS DIFF THAN 0 BECAUSE OF SQL CONDITION
			// BUT I ADDED JUST IN CASE IF WE CHANGE SQL AND FORGOT HERE.
			placementRatio = double1; 
		}
	}
	
	/*public void worstErasmus_multiple(int int1) {
		if (erasmus_multiple > int1) {
			erasmus_multiple = int1;
		}
	}*/

}

