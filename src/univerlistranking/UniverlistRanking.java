
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import konjejyan.Akademisyen;
import konjejyan.DisabledStudent;
import konjejyan.ExStudent;
import konjejyan.ForeignAkademisyen;
import konjejyan.ForeignStudent;
import konjejyan.Girisimcilik;
import static konjejyan.KontejyanExcel.*;
import konjejyan.Student;
import konjejyan.URAP;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class UniverlistRanking {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        boolean update = true; // Boomsocial updatei için true, güncel veriler için false
        ArrayList<University> universite = new ArrayList<University>();
        ArrayList<BoomSocial> boomveri = new ArrayList<BoomSocial>();
        ArrayList<ForeignAkademisyen> yabanciakademisyen = new ArrayList<ForeignAkademisyen>();
        ArrayList<Akademisyen> akademisyen = new ArrayList<Akademisyen>();
        ArrayList<Student> ogrenci = new ArrayList<Student>();
        ArrayList<DisabledStudent> engelliogrenci = new ArrayList<DisabledStudent>();
        ArrayList<ForeignStudent> yabanciogrenci = new ArrayList<ForeignStudent>();
        ArrayList<ExStudent> degisimogrenci = new ArrayList<ExStudent>();
        ArrayList<URAP> urap = new ArrayList<URAP>();
        ArrayList<Girisimcilik> girisimcilik = new ArrayList<Girisimcilik>();
                
        File file = new File("dosya-new.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(file, false);
        BufferedWriter bWriter = new BufferedWriter(fileWriter);
        try {
            
            Connection c = null;
            Statement stmt = null;
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://46.101.220.50:5432/univerlist_2018",
                            "univerlist_user", "univer2017DB.");
            System.out.println("Opened database successfully");
            stmt = c.createStatement();
            String sql;
            if(update){
                yabanciakademisyen = YabanciAkademisyenParser("excel/2017_T35.xlsx",false);//(?)
                akademisyen = AkademisyenParser("excel/Akademisyen_Sayıları.xlsx",false);//(?)
                ogrenci = OgrenciSayisiParser("excel/ÖğrenciSayıları.xlsx",false);
                engelliogrenci = EngelliOgrenciSayisiParser("excel/EngelliÖğrenciSayıları.xlsx",false);//(?)
                yabanciogrenci = UyrugaGoreOgrenciSayisiParser("excel/UyruğaGöreÖğrenciSayıları.xlsx",false);//(?)
                degisimogrenci = DegisimOgrenciSayisiParser("excel/DeğişimÖğrenciSayıları.xlsx", false);
                urap = URAPParser("excel/URAP.xlsx",false);
                girisimcilik = GirisimcilikParser("excel/2017_gyue_siralama.xlsx",false);
                boomveri = BoomSocialParser();
            }
                
            
            ResultSet results = stmt.executeQuery("Select * from univerlist_app_university INNER JOIN univerlist_app_university_translation u ON univerlist_app_university.id = u.master_id INNER JOIN univerlist_app_universityrating_copy uau ON univerlist_app_university.id = uau.university_id");
            while (results.next()) {
                universite.add(new University(results.getString("name"), results.getInt("id")));
                
            }
            SuperUniversity superUniversity = new SuperUniversity();
            WorstUniversity worstUniversity = new WorstUniversity();
            for (University university : universite) {
                if(update){
                    //Bazı yerlerde düzeltmeler olacak toplamını yazmak vb gibi. Generic method yazılabilir(?)
                    UpdateUniversityData(university.getName(),findIndexGirisimcilik(girisimcilik, university.getName()),findIndexURAP(urap, university.getName()),findIndexExStudent(degisimogrenci, university.getName()),findIndexBoomsocial(boomveri,university.getName()),findIndexOgrenci(ogrenci,university.getName()),findIndexYabanciOgrenci(yabanciogrenci,university.getName()),findIndexEngelliOgrenci(engelliogrenci,university.getName()),findIndexAkademisyen(akademisyen,university.getName()), findIndexYabanciAkademisyen(yabanciakademisyen,university.getName()), stmt);
                }
                sql = "select * from univerlist_app_universityrating_copy where university_id =" + university.getId();
                results = stmt.executeQuery(sql);
                results.next();
                university.setErasmus_multiple(results.getInt("erasmus_income") + results.getInt("erasmus_outcome"));
                university.setBoomsocial(new BoomSocial(results.getInt("boomsocial_facebook_interaction"), results.getInt("boomsocial_facebook_follower"), results.getInt("boomsocial_facebook_score"), results.getInt("boomsocial_twitter_interaction"), results.getInt("boomsocial_twitter_follower"), results.getInt("boomsocial_twitter_score"), results.getInt("boomsocial_instagram_interaction"), results.getInt("boomsocial_instagram_follower"), results.getInt("boomsocial_instagram_score")));
                university.setDisabledStudent(results.getInt("disabled_student"));
                university.setDocent(results.getInt("docent"));
                university.setDocentYard(results.getInt("docent_yard"));
                university.setEntrepreneurship(results.getDouble("entrepreneurship"));
                university.setExchangeStudents(new ExchangeStudent(results.getInt("student_exchange"), results.getInt("erasmus_income"), results.getInt("erasmus_outcome"), results.getInt("farabi_income"), results.getInt("farabi_outcome"), results.getInt("mevlana_income"), results.getInt("mevlana_outcome")));
                university.setForeignInstructor(results.getInt("foreign_instructor"));
                university.setForeignStudent(results.getInt("foreign_student"));
                university.setInstructor(results.getInt("instructor"));
                university.setProf(results.getInt("prof"));
                university.setStudents(new StudentNumbers(results.getInt("associate_woman"), results.getInt("associate_man"), results.getInt("bachelor_woman"), results.getInt("bachelor_man"), results.getInt("master_woman"), results.getInt("master_man"), results.getInt("doctorate_woman"), results.getInt("doctorate_man"), results.getInt("total_woman"), results.getInt("total_man")));
                university.setUrapScores(new UrapPoints(results.getDouble("urap_article"), results.getDouble("urap_citation"), results.getDouble("urap_document"), results.getDouble("urap_instructor"), results.getDouble("urap_doktorate")));

                superUniversity.superErasmus_multiple(results.getInt("erasmus_income") + results.getInt("erasmus_outcome"));
                superUniversity.superBoomsocial(results.getInt("boomsocial_facebook_interaction"), results.getInt("boomsocial_facebook_follower"), results.getInt("boomsocial_facebook_score"), results.getInt("boomsocial_twitter_interaction"), results.getInt("boomsocial_twitter_follower"), results.getInt("boomsocial_twitter_score"), results.getInt("boomsocial_instagram_interaction"), results.getInt("boomsocial_instagram_follower"), results.getInt("boomsocial_instagram_score"));
                superUniversity.superDisabledStudent(results.getInt("disabled_student"));
                superUniversity.superDocent(results.getInt("docent"));
                superUniversity.superDocentYard(results.getInt("docent_yard"));
                superUniversity.superEntrepreneurship(results.getDouble("entrepreneurship"));
                superUniversity.superExchangeStudents(results.getInt("student_exchange"), results.getInt("erasmus_income"), results.getInt("erasmus_outcome"), results.getInt("farabi_income"), results.getInt("farabi_outcome"), results.getInt("mevlana_income"), results.getInt("mevlana_outcome"));
                superUniversity.superForeignInstructor(results.getInt("foreign_instructor"));
                superUniversity.superForeignStudent(results.getInt("foreign_student"));
                superUniversity.superInstructor(results.getInt("instructor"));
                superUniversity.superProf(results.getInt("prof"));
                superUniversity.superStudents(results.getInt("associate_woman"), results.getInt("associate_man"), results.getInt("bachelor_woman"), results.getInt("bachelor_man"), results.getInt("master_woman"), results.getInt("master_man"), results.getInt("doctorate_woman"), results.getInt("doctorate_man"), results.getInt("total_woman"), results.getInt("total_man"));
                superUniversity.superUrapScores(results.getDouble("urap_article"), results.getDouble("urap_citation"), results.getDouble("urap_document"), results.getDouble("urap_instructor"), results.getDouble("urap_doktorate"));

                worstUniversity.worstBoomsocial(results.getInt("boomsocial_facebook_interaction"), results.getInt("boomsocial_facebook_follower"), results.getInt("boomsocial_facebook_score"), results.getInt("boomsocial_twitter_interaction"), results.getInt("boomsocial_twitter_follower"), results.getInt("boomsocial_twitter_score"), results.getInt("boomsocial_instagram_interaction"), results.getInt("boomsocial_instagram_follower"), results.getInt("boomsocial_instagram_score"));
                worstUniversity.worstDisabledStudent(results.getInt("disabled_student"));
                worstUniversity.worstDocent(results.getInt("docent"));
                worstUniversity.worstDocentYard(results.getInt("docent_yard"));
                worstUniversity.worstEntrepreneurship(results.getDouble("entrepreneurship"));
                worstUniversity.worstExchangeStudents(results.getInt("student_exchange"), results.getInt("erasmus_income"), results.getInt("erasmus_outcome"), results.getInt("farabi_income"), results.getInt("farabi_outcome"), results.getInt("mevlana_income"), results.getInt("mevlana_outcome"));
                worstUniversity.worstForeignInstructor(results.getInt("foreign_instructor"));
                worstUniversity.worstForeignStudent(results.getInt("foreign_student"));
                worstUniversity.worstInstructor(results.getInt("instructor"));
                worstUniversity.worstProf(results.getInt("prof"));
                worstUniversity.worstStudents(results.getInt("associate_woman"), results.getInt("associate_man"), results.getInt("bachelor_woman"), results.getInt("bachelor_man"), results.getInt("master_woman"), results.getInt("master_man"), results.getInt("doctorate_woman"), results.getInt("doctorate_man"), results.getInt("total_woman"), results.getInt("total_man"));
                worstUniversity.worstUrapScores(results.getDouble("urap_article"), results.getDouble("urap_citation"), results.getDouble("urap_document"), results.getDouble("urap_instructor"), results.getDouble("urap_doktorate"));

                sql = "SELECT count(*) as featureTotal,university_id FROM public.univerlist_app_university_features  where university_id=" + university.getId() + " group by university_id";
                results = stmt.executeQuery(sql);
                // NEED IF CONDITION AS A CHECKER BECAUSE THERE WILL BE A UNIVERSITY WHICH DOES NOT HAVE ANY FEATURES BECAUSE ITS NEWLY ADDED ETC. 
                if (results.next()) {
                    university.setFeatures(results.getInt("featureTotal"));
                    superUniversity.superFeatures(results.getInt("featureTotal"));
                    worstUniversity.worstFeatures(results.getInt("featureTotal"));
                }

                sql = "SELECT technopark,incubation_center from univerlist_app_university where id = " + university.getId();
                results = stmt.executeQuery(sql);
                if (results.next()) {
                    if (results.getBoolean("technopark")) {
                        university.setTechnopark(1); // Default value of technopark is 0 so no need to else.
                    }
                    if (results.getBoolean("incubation_center")) {
                        university.setIncubationCenter(1);
                    }
                }

                sql = "SELECT count(*) as totalFavorite,university_id FROM public.univerlist_app_favorite where university_id=" + university.getId() + " group by university_id";
                results = stmt.executeQuery(sql);
                if (results.next()) {
                    university.setFavorite(results.getInt("totalFavorite"));
                    superUniversity.superFavorite(results.getInt("totalFavorite"));
                    worstUniversity.worstFavorite(results.getInt("totalFavorite"));
                }

                sql = "SELECT sum(score) as totalScore,target_university_id FROM public.univerlist_app_comment where is_approved=true and target_university_id=" + university.getId() + " group by target_university_id";
                results = stmt.executeQuery(sql);
                if (results.next()) {
                    university.setCommentScore(results.getInt("totalScore"));
                    superUniversity.superCommentScore(results.getInt("totalScore"));
                    // ATTENTION HERE :  i did not add the worst university because we only have 1 university with scores so worst score will be the best score at the moment.
                }

                sql = "SELECT placement_rate from univerlist_app_university where placement_rate > 0 and id = " + university.getId();
                results = stmt.executeQuery(sql);
                if (results.next()) {
                    university.setPlacementRatio(results.getDouble("placement_rate"));
                    superUniversity.superPlacementRatio(results.getDouble("placement_rate"));
                    worstUniversity.worstPlacementRatio(results.getDouble("placement_rate"));
                }

                //university.calculateRatios();
                //superUniversity.superRatios(university.getProfStudentRatio(),university.getDocentStudentRatio(),university.getDocentYardStudentRatio(),university.getInstructorStudentRatio(),university.getForeignStudentRatio(),university.getForeignInstructorRatio(),university.getDisabledStudentRatio(),university.getFacebookFollowerRatio(),university.getTwitterFollowerRatio(),university.getInstagramFollowerRatio(),university.getErasmusOutcomeRatio(),university.getOtherExchangeRatio());
            }
            //System.out.println(worstUniversity.toString());
            for (University university : universite) {
                university.checkWorstCases(worstUniversity);
                university.calculateRatios();
                superUniversity.superRatios(university.getProfStudentRatio(), university.getDocentStudentRatio(), university.getDocentYardStudentRatio(), university.getInstructorStudentRatio(), university.getForeignStudentRatio(), university.getForeignInstructorRatio(), university.getDisabledStudentRatio(), university.getFacebookFollowerRatio(), university.getTwitterFollowerRatio(), university.getInstagramFollowerRatio(), university.getErasmusOutcomeRatio(), university.getOtherExchangeRatio());

            }
            double TotalAvgScore = 0;
            int count = 0;
            superUniversity.calculateAverageScore(superUniversity, 29.36, 32.03, 14.58, 14.68, 9.34, 0);
            double superAvg = superUniversity.getAvgScore();
            //System.out.println("superAvg = " + superAvg);
            double maxIMDB = -100;
            double minIMDB = 999;
            for (University university : universite) {
                count++;
                university.calculateAverageScore(superUniversity, 29.36, 32.03, 14.58, 14.68, 9.34, 0);
                TotalAvgScore += university.getAvgScore();
                double temp = university.calculateIMDBScore(superAvg);
                if (temp < minIMDB) {
                    //minIMDB = Double.parseDouble(new DecimalFormat("##.##").format(temp));
                    minIMDB = temp;
                }
                if (temp > maxIMDB) {
                    maxIMDB = temp;
                }
            }
            double OrtalamaAVGScore = TotalAvgScore / count; // bu nasıl bi isimlendirme...
            double standartSapma = (OrtalamaAVGScore - superAvg) / superAvg;
            double stDEV = 1 - standartSapma;
            //System.out.println("Min =" + minIMDB + " Max = " + maxIMDB);
            //System.out.println("OrtalamaAVGScore = "+OrtalamaAVGScore+" standartSapma = "+standartSapma+" stDEV = "+stDEV);
            for (University university : universite) {
                university.imdbWithoutDeviation(standartSapma, stDEV, superAvg, minIMDB, maxIMDB);
                System.out.println("University Name = "+university.getName()+" IMDB SCORE = "+university.getIMDBScore());
                bWriter.write("University Name = "+university.getName()+" IMDB SCORE = "+university.getIMDBScore());
                bWriter.newLine();
             //  sql = "UPDATE univerlist_app_university SET ul_score = " + university.getIMDBScore() + " where id = " + university.getId();
             //   int updated = stmt.executeUpdate(sql);
                /*if(updated==1)
				    	 System.out.println("Updated "+university.getName());
				     else{
				    	 System.out.println("Failed "+university.getName());
				     }*/
            }
            stmt.close();
            c.close();
            bWriter.close();
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private static void UpdateUniversityInfo(String universiteAdi, double girisimcilikPuani, Statement stmt) throws SQLException {
        int uniID = getUniversityID(universiteAdi, stmt);
        if (uniID == 0) {
            System.out.println(universiteAdi + " is not on our DB.");
            return;
        }
        String sql = "UPDATE univerlist_app_universityrating SET entrepreneurship=" + girisimcilikPuani + " WHERE university_id =" + uniID;
        int updated = stmt.executeUpdate(sql);
        if (updated == 0) {
            System.out.println("Failed " + universiteAdi);
        } else {
            System.out.println("Success " + universiteAdi);
        }
    }
    
    private static void UpdateUniversityData(String universiteAdi,ArrayList<Girisimcilik> girisimcilik,ArrayList<URAP> urap,ArrayList<ExStudent> degisimogrenci ,ArrayList<BoomSocial> boomsocial,ArrayList<Student> ogrenci,ArrayList<ForeignStudent> yabanciogrenci,ArrayList<DisabledStudent> engelliogrenci,ArrayList<Akademisyen> akademisyen,ArrayList<ForeignAkademisyen> yabanciakademisyen, Statement stmt) throws SQLException {
        int uniID = getUniversityID(universiteAdi, stmt);
        if (uniID == 0) {
            System.out.println(universiteAdi + " is not on our DB.");
            return;
        }
       /* String sql = "UPDATE univerlist_app_universityrating_copy SET associate_man="+ogrenci.get(0).getOnlisanserkek()+",associate_woman="+ogrenci.get(0).getOnlisanskadin()
                +",bachelor_man="+ogrenci.get(0).getLisanserkek()+",bachelor_woman="+ogrenci.get(0).getLisanskadin()+",master_man="+ogrenci.get(0).getYlerkek()+",master_woman="+ogrenci.get(0).getYlkadin()
                +",doctorate_man="+ogrenci.get(0).getDoktoraerkek()+",doctorate_woman="+ogrenci.get(0).getDoktorakadin()+",total_man="+ogrenci.get(0).getTotalerkek()+",total_woman="+ogrenci.get(0).getTotalkadin()
                +",prof = "+ akademisyen.get(0).getProf()+",instructor="+akademisyen.get(0).getInstructor()
                +",lecturer="+akademisyen.get(0).getLecturer()+",urap_article="+urap.get(0).getUrap_article()+",urap_citation="+urap.get(0).getUrap_citation()+",urap_document="+urap.get(0).getUrap_document()
                +",urap_doktorate="+urap.get(0).getUrap_doktorate()+",urap_instructor="+urap.get(0).getUrap_instructor()+",entrepreneurship="+girisimcilik.get(0).getTotal()
                +",docent="+akademisyen.get(0).getDoc()+",docent_yard = "+akademisyen.get(0).getYardoc()+",foreign_student = "+yabanciogrenci.get(0).getNumber()
                +",foreign_instructor="+yabanciakademisyen.get(0).getTotal()+",disabled_student="+engelliogrenci.get(0).getNumber()+",erasmus_income="+degisimogrenci.get(0).getErasmusGelen()
                +",erasmus_outcome="+degisimogrenci.get(0).getErasmusGiden()+",farabi_income="+degisimogrenci.get(0).getFarabiGelen()+",farabi_outcome="+degisimogrenci.get(0).getFarabiGiden()
                +",mevlana_income="+degisimogrenci.get(0).getMevlanaGelen()+",mevlana_outcome="+degisimogrenci.get(0).getMevlanaGiden()+",student_exchange="+degisimogrenci.get(0).getExchangedStudent()
                +" WHERE university_id =" + uniID;*/

        
        String sql = "UPDATE univerlist_app_universityrating_copy SET boomsocial_instagram_score=" + boomsocial.get(0).getBoomsocialInstagramScore() + ",boomsocial_facebook_score=" 
                + boomsocial.get(0).getBoomsocialFacebookScore()+",boomsocial_twitter_score="+boomsocial.get(0).getBoomsocialTwitterScore()
                +",boomsocial_instagram_follower="+boomsocial.get(0).getBoomsocialInstagramFollower()+",boomsocial_instagram_interaction="+boomsocial.get(0).getBoomsocialInstagramInteraction()
                +",boomsocial_facebook_follower="+boomsocial.get(0).getBoomsocialFacebookFollower()+",boomsocial_facebook_interaction="+boomsocial.get(0).getBoomsocialFacebookInteraction()
                +",boomsocial_twitter_follower="+boomsocial.get(0).getBoomsocialTwitterFollower()+",associate_man="+ogrenci.get(0).getOnlisanserkek()+",associate_woman="+ogrenci.get(0).getOnlisanskadin()
                +",bachelor_man="+ogrenci.get(0).getLisanserkek()+",bachelor_woman="+ogrenci.get(0).getLisanskadin()+",master_man="+ogrenci.get(0).getYlerkek()+",master_woman="+ogrenci.get(0).getYlkadin()
                +",doctorate_man="+ogrenci.get(0).getDoktoraerkek()+",doctorate_woman="+ogrenci.get(0).getDoktorakadin()+",total_man="+ogrenci.get(0).getTotalerkek()+",total_woman="+ogrenci.get(0).getTotalkadin()
                +",boomsocial_twitter_interaction="+boomsocial.get(0).getBoomsocialTwitterInteraction()+",prof = "+ akademisyen.get(0).getProf()+",instructor="+akademisyen.get(0).getInstructor()
                +",lecturer="+akademisyen.get(0).getLecturer()+",urap_article="+urap.get(0).getUrap_article()+",urap_citation="+urap.get(0).getUrap_citation()+",urap_document="+urap.get(0).getUrap_document()
                +",urap_doktorate="+urap.get(0).getUrap_doktorate()+",urap_instructor="+urap.get(0).getUrap_instructor()+",entrepreneurship="+girisimcilik.get(0).getTotal()
                +",docent="+akademisyen.get(0).getDoc()+",docent_yard = "+akademisyen.get(0).getYardoc()+",foreign_student = "+yabanciogrenci.get(0).getNumber()
                +",foreign_instructor="+yabanciakademisyen.get(0).getTotal()+",disabled_student="+engelliogrenci.get(0).getNumber()+",erasmus_income="+degisimogrenci.get(0).getErasmusGelen()
                +",erasmus_outcome="+degisimogrenci.get(0).getErasmusGiden()+",farabi_income="+degisimogrenci.get(0).getFarabiGelen()+",farabi_outcome="+degisimogrenci.get(0).getFarabiGiden()
                +",mevlana_income="+degisimogrenci.get(0).getMevlanaGelen()+",mevlana_outcome="+degisimogrenci.get(0).getMevlanaGiden()+",student_exchange="+degisimogrenci.get(0).getExchangedStudent()
                +" WHERE university_id =" + uniID;
        
        int updated = stmt.executeUpdate(sql);
        if (updated == 0) {
            System.out.println("Failed " + universiteAdi);
        } /*else {
            System.out.println("Success " + universiteAdi);
        }*/
    }

    private static int getUniversityID(String universiteAdi, Statement stmt) {
        int result = 0;
        try {
            String sql = "SELECT * FROM univerlist_app_university_translation WHERE name='" + universiteAdi + "'";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (result == 0) {
            System.out.println("Couldnt get the id of " + universiteAdi);
            //pauseCode("Couldnt get the id of "+universiteAdi);
        }
        return result;
    }

    public static void pauseCode(String a) {
        System.out.println(a);
        try {
            System.in.read();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private static ArrayList<BoomSocial> BoomSocialParser() throws FileNotFoundException, IOException{
        ArrayList<BoomSocial> list = new ArrayList<BoomSocial>();
        String[] social = {"boomsocial.xlsx"};
            FileInputStream file = new FileInputStream("excel/"+social[0]);
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if(row.getCell(0) != null){
                    BoomSocial boomsocial = new BoomSocial();
                    boomsocial.setUniAdi(row.getCell(0).getStringCellValue());
                    boomsocial.setBoomsocialFacebookScore((int) Double.parseDouble(row.getCell(4).getStringCellValue()));
                    boomsocial.setBoomsocialFacebookFollower(Integer.parseInt(row.getCell(5).getStringCellValue()));
                    boomsocial.setBoomsocialFacebookInteraction(Integer.parseInt(row.getCell(6).getStringCellValue()));
                    boomsocial.setBoomsocialTwitterScore((int) Double.parseDouble(row.getCell(1).getStringCellValue()));
                    boomsocial.setBoomsocialTwitterFollower(Integer.parseInt(row.getCell(2).getStringCellValue()));
                    boomsocial.setBoomsocialTwitterInteraction(Integer.parseInt(row.getCell(3).getStringCellValue()));
                    boomsocial.setBoomsocialInstagramScore((int) Double.parseDouble(row.getCell(7).getStringCellValue()));
                    boomsocial.setBoomsocialInstagramFollower(Integer.parseInt(row.getCell(8).getStringCellValue()));
                    boomsocial.setBoomsocialInstagramInteraction(Integer.parseInt(row.getCell(9).getStringCellValue()));
                    list.add(boomsocial);
                }
            }
        return list;
    }

    private static ArrayList<BoomSocial> findIndexBoomsocial(ArrayList<BoomSocial> boomveri,String uniAdi) { 
        ArrayList<BoomSocial> list = new ArrayList<BoomSocial>();
        for(int i =0;i<boomveri.size();i++){
            if(boomveri.get(i).getUniAdi().equals(uniAdi)){
                list.add(boomveri.get(i));
                break;
            }
        }
        BoomSocial e = new BoomSocial();
        list.add(e);
        return list;
    }

    private static ArrayList<Student> findIndexOgrenci(ArrayList<Student> ogrenci, String uniAdi) {
        ArrayList<Student> list = new ArrayList<Student>();
        for(int i =0;i<ogrenci.size();i++){
            if(ogrenci.get(i).getUniAdi().equals(uniAdi)){
                list.add(ogrenci.get(i));
                break;
            }
        }
        Student e = new Student();
        list.add(e);
        return list;
    }

    private static ArrayList<ForeignStudent> findIndexYabanciOgrenci(ArrayList<ForeignStudent> yabanciogrenci, String uniAdi) {
        ArrayList<ForeignStudent> list = new ArrayList<ForeignStudent>();
        for(int i =0;i<yabanciogrenci.size();i++){
            if(yabanciogrenci.get(i).getUniAdi().equals(uniAdi)){
                list.add(yabanciogrenci.get(i));
                return list;
            }
        }
        ForeignStudent e = new ForeignStudent();
        list.add(e);
        return list;
    }

    private static ArrayList<DisabledStudent> findIndexEngelliOgrenci(ArrayList<DisabledStudent> engelliogrenci, String uniAdi) {
        ArrayList<DisabledStudent> list = new ArrayList<DisabledStudent>();
        for(int i =0;i<engelliogrenci.size();i++){
            if(engelliogrenci.get(i).getUniAdi().equals(uniAdi)){
                list.add(engelliogrenci.get(i));
                return list;
            }
        }
        DisabledStudent e = new DisabledStudent();
        list.add(e);
        return list;
    }

    private static ArrayList<Akademisyen> findIndexAkademisyen(ArrayList<Akademisyen> akademisyen, String uniAdi) {
        ArrayList<Akademisyen> list = new ArrayList<Akademisyen>();
        for(int i =0;i<akademisyen.size();i++){
            if(akademisyen.get(i).getUniAdi().equals(uniAdi)){
                list.add(akademisyen.get(i));
                return list;
            }
        }
       Akademisyen e = new Akademisyen();
       list.add(e);
       return list; 
    }

    private static ArrayList<ExStudent> findIndexExStudent(ArrayList<ExStudent> degisimogrenci, String uniAdi) {
        ArrayList<ExStudent> list = new ArrayList<ExStudent>();
        for(int i =0;i<degisimogrenci.size();i++){
            if(degisimogrenci.get(i).getUniAdi().equals(uniAdi)){
                list.add(degisimogrenci.get(i));
                return list;
            }
        }
        ExStudent e = new ExStudent();
        list.add(e);
        return list;
    }
    
    private static ArrayList<ForeignAkademisyen> findIndexYabanciAkademisyen(ArrayList<ForeignAkademisyen> yabanciakademisyen, String uniAdi) {
        ArrayList<ForeignAkademisyen> list = new ArrayList<ForeignAkademisyen>();
        for(int i =0;i<yabanciakademisyen.size();i++){
            if(yabanciakademisyen.get(i).getUniAdi().equals(uniAdi)){
                list.add(yabanciakademisyen.get(i));
                return list;
            }
        }
        ForeignAkademisyen e = new ForeignAkademisyen();
        list.add(e);
        return list;
    }
    
    private static ArrayList<URAP> findIndexURAP(ArrayList<URAP> urap, String uniAdi) {
        ArrayList<URAP> list = new ArrayList<URAP>();
        for(int i =0;i<urap.size();i++){
            if(urap.get(i).getUniAdi().equals(uniAdi)){
                list.add(urap.get(i));
                return list;
            }
        }
        URAP e = new URAP();
        list.add(e);
        return list;
    }
    
    private static ArrayList<Girisimcilik> findIndexGirisimcilik(ArrayList<Girisimcilik> girisimcilik, String uniAdi) {
        ArrayList<Girisimcilik> list = new ArrayList<Girisimcilik>();
        for(int i =0;i<girisimcilik.size();i++){
            if(girisimcilik.get(i).getUniAdi().equals(uniAdi)){
                list.add(girisimcilik.get(i));
                return list;
            }
        }
        Girisimcilik e = new Girisimcilik();
        list.add(e);
        return list;
    }
}
