package univerlistranking;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
 
public class BoomSocial {
        private String uniAdi;
    
	private int boomsocialFacebookInteraction;
	private int boomsocialFacebookFollower;
	private int boomsocialFacebookScore;
	
	private int boomsocialTwitterInteraction;
	private int boomsocialTwitterFollower;
	private int boomsocialTwitterScore;

	
	private int boomsocialInstagramInteraction;
	private int boomsocialInstagramFollower;
	private int boomsocialInstagramScore;
	
	public BoomSocial(int boomsocialFacebookInteraction, int boomsocialFacebookFollower, int boomsocialFacebookScore,
			int boomsocialTwitterInteraction, int boomsocialTwitterFollower, int boomsocialTwitterScore,
			int boomsocialInstagramInteraction, int boomsocialInstagramFollower, int boomsocialInstagramScore) {
		this.boomsocialFacebookInteraction = boomsocialFacebookInteraction;
		this.boomsocialFacebookFollower = boomsocialFacebookFollower;
		this.boomsocialFacebookScore = boomsocialFacebookScore;
		this.boomsocialTwitterInteraction = boomsocialTwitterInteraction;
		this.boomsocialTwitterFollower = boomsocialTwitterFollower;
		this.boomsocialTwitterScore = boomsocialTwitterScore;
		this.boomsocialInstagramInteraction = boomsocialInstagramInteraction;
		this.boomsocialInstagramFollower = boomsocialInstagramFollower;
		this.boomsocialInstagramScore = boomsocialInstagramScore;
	}

        public BoomSocial() {
        }

	public int getBoomsocialFacebookInteraction() {
		return boomsocialFacebookInteraction;
	}

	public int getBoomsocialFacebookFollower() {
		return boomsocialFacebookFollower;
	}

	public int getBoomsocialFacebookScore() {
		return boomsocialFacebookScore;
	}

	public int getBoomsocialTwitterInteraction() {
		return boomsocialTwitterInteraction;
	}

	public int getBoomsocialTwitterFollower() {
		return boomsocialTwitterFollower;
	}

	public int getBoomsocialTwitterScore() {
		return boomsocialTwitterScore;
	}

	public int getBoomsocialInstagramInteraction() {
		return boomsocialInstagramInteraction;
	}

	public int getBoomsocialInstagramFollower() {
		return boomsocialInstagramFollower;
	}

	public int getBoomsocialInstagramScore() {
		return boomsocialInstagramScore;
	}
        
        public void setBoomsocialFacebookInteraction(int boomsocialFacebookInteraction) {
        this.boomsocialFacebookInteraction = boomsocialFacebookInteraction;
        }

        public void setBoomsocialFacebookFollower(int boomsocialFacebookFollower) {
            this.boomsocialFacebookFollower = boomsocialFacebookFollower;
        }

        public void setBoomsocialFacebookScore(int boomsocialFacebookScore) {
            this.boomsocialFacebookScore = boomsocialFacebookScore;
        }

        public void setBoomsocialTwitterInteraction(int boomsocialTwitterInteraction) {
            this.boomsocialTwitterInteraction = boomsocialTwitterInteraction;
        }

        public void setBoomsocialTwitterFollower(int boomsocialTwitterFollower) {
            this.boomsocialTwitterFollower = boomsocialTwitterFollower;
        }

        public void setBoomsocialTwitterScore(int boomsocialTwitterScore) {
            this.boomsocialTwitterScore = boomsocialTwitterScore;
        }

        public void setBoomsocialInstagramInteraction(int boomsocialInstagramInteraction) {
            this.boomsocialInstagramInteraction = boomsocialInstagramInteraction;
        }

        public void setBoomsocialInstagramFollower(int boomsocialInstagramFollower) {
            this.boomsocialInstagramFollower = boomsocialInstagramFollower;
        }

        public void setBoomsocialInstagramScore(int boomsocialInstagramScore) {
            this.boomsocialInstagramScore = boomsocialInstagramScore;
        }
	
	public void superBoomSocial(int boomsocialFacebookInteraction, int boomsocialFacebookFollower, int boomsocialFacebookScore,
			int boomsocialTwitterInteraction, int boomsocialTwitterFollower, int boomsocialTwitterScore,
			int boomsocialInstagramInteraction, int boomsocialInstagramFollower, int boomsocialInstagramScore) {
		if(this.boomsocialFacebookInteraction < boomsocialFacebookInteraction)
		this.boomsocialFacebookInteraction = boomsocialFacebookInteraction;
		if(this.boomsocialFacebookFollower < boomsocialFacebookFollower)
		this.boomsocialFacebookFollower = boomsocialFacebookFollower;
		if(this.boomsocialFacebookScore < boomsocialFacebookScore)
		this.boomsocialFacebookScore = boomsocialFacebookScore;
		if(this.boomsocialTwitterInteraction < boomsocialTwitterInteraction)
		this.boomsocialTwitterInteraction = boomsocialTwitterInteraction;
		if(this.boomsocialTwitterFollower < boomsocialTwitterFollower)
		this.boomsocialTwitterFollower = boomsocialTwitterFollower;
		if(this.boomsocialTwitterScore < boomsocialTwitterScore)
		this.boomsocialTwitterScore = boomsocialTwitterScore;
		if(this.boomsocialInstagramInteraction < boomsocialInstagramInteraction)
		this.boomsocialInstagramInteraction = boomsocialInstagramInteraction;
		if(this.boomsocialInstagramFollower < boomsocialInstagramFollower)
		this.boomsocialInstagramFollower = boomsocialInstagramFollower;
		if(this.boomsocialInstagramScore < boomsocialInstagramScore)
		this.boomsocialInstagramScore = boomsocialInstagramScore;
	}
	
	public void worstBoomSocial(int boomsocialFacebookInteraction, int boomsocialFacebookFollower, int boomsocialFacebookScore,
			int boomsocialTwitterInteraction, int boomsocialTwitterFollower, int boomsocialTwitterScore,
			int boomsocialInstagramInteraction, int boomsocialInstagramFollower, int boomsocialInstagramScore) {
		if(this.boomsocialFacebookInteraction > boomsocialFacebookInteraction && boomsocialFacebookInteraction!=0)
		this.boomsocialFacebookInteraction = boomsocialFacebookInteraction;
		if(this.boomsocialFacebookFollower > boomsocialFacebookFollower && boomsocialFacebookFollower!=0)
		this.boomsocialFacebookFollower = boomsocialFacebookFollower;
		if(this.boomsocialFacebookScore > boomsocialFacebookScore && boomsocialFacebookScore!=0)
		this.boomsocialFacebookScore = boomsocialFacebookScore;
		if(this.boomsocialTwitterInteraction > boomsocialTwitterInteraction && boomsocialTwitterInteraction!=0)
		this.boomsocialTwitterInteraction = boomsocialTwitterInteraction;
		if(this.boomsocialTwitterFollower > boomsocialTwitterFollower && boomsocialTwitterFollower!=0)
		this.boomsocialTwitterFollower = boomsocialTwitterFollower;
		if(this.boomsocialTwitterScore > boomsocialTwitterScore && boomsocialTwitterScore!=0)
		this.boomsocialTwitterScore = boomsocialTwitterScore;
		if(this.boomsocialInstagramInteraction > boomsocialInstagramInteraction && boomsocialInstagramInteraction!=0)
		this.boomsocialInstagramInteraction = boomsocialInstagramInteraction;
		if(this.boomsocialInstagramFollower > boomsocialInstagramFollower && boomsocialInstagramFollower!=0)
		this.boomsocialInstagramFollower = boomsocialInstagramFollower;
		if(this.boomsocialInstagramScore > boomsocialInstagramScore && boomsocialInstagramScore!=0)
		this.boomsocialInstagramScore = boomsocialInstagramScore;
	}

	public void checkWorstCases(BoomSocial boomsocial) {
		if(this.boomsocialFacebookInteraction == 0)
			this.boomsocialFacebookInteraction = boomsocial.getBoomsocialFacebookInteraction() /2;
			if(this.boomsocialFacebookFollower == 0)
			this.boomsocialFacebookFollower = boomsocial.getBoomsocialFacebookFollower() /2;
			if(this.boomsocialFacebookScore == 0)
			this.boomsocialFacebookScore = boomsocial.getBoomsocialFacebookScore() /2;
			if(this.boomsocialTwitterInteraction == 0)
			this.boomsocialTwitterInteraction = boomsocial.getBoomsocialTwitterInteraction() /2;
			if(this.boomsocialTwitterFollower == 0)
			this.boomsocialTwitterFollower = boomsocial.getBoomsocialTwitterFollower() /2;
			if(this.boomsocialTwitterScore == 0)
			this.boomsocialTwitterScore = boomsocial.getBoomsocialTwitterScore() /2;
			if(this.boomsocialInstagramInteraction == 0)
			this.boomsocialInstagramInteraction = boomsocial.getBoomsocialInstagramInteraction() /2;
			if(this.boomsocialInstagramFollower == 0)
			this.boomsocialInstagramFollower = boomsocial.getBoomsocialInstagramFollower() /2;
			if(this.boomsocialInstagramScore == 0)
			this.boomsocialInstagramScore = boomsocial.getBoomsocialInstagramScore() /2;
	}
        
        public String getUniAdi() {
            return uniAdi;
        }

        public void setUniAdi(String uniAdi) {
            this.uniAdi = uniAdi;
        }

	@Override
	public String toString() {
		return "BoomSocial [boomsocialFacebookInteraction=" + boomsocialFacebookInteraction
				+ ", boomsocialFacebookFollower=" + boomsocialFacebookFollower + ", boomsocialFacebookScore="
				+ boomsocialFacebookScore + ", boomsocialTwitterInteraction=" + boomsocialTwitterInteraction
				+ ", boomsocialTwitterFollower=" + boomsocialTwitterFollower + ", boomsocialTwitterScore="
				+ boomsocialTwitterScore + ", boomsocialInstagramInteraction=" + boomsocialInstagramInteraction
				+ ", boomsocialInstagramFollower=" + boomsocialInstagramFollower + ", boomsocialInstagramScore="
				+ boomsocialInstagramScore + "]";
	}
	
	
	
}

