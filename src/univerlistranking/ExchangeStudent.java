/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */
 
public class ExchangeStudent {
	int exchangedStudent;
	int erasmusGelen;
	int erasmusGiden;
	int farabiGelen;
	int farabiGiden;
	int mevlanaGelen;
	int mevlanaGiden;
	public ExchangeStudent(int exchangedStudent, int erasmusGelen, int erasmusGiden, int farabiGelen, int farabiGiden,
			int mevlanaGelen, int mevlanaGiden) {
		this.exchangedStudent = exchangedStudent;
		this.erasmusGelen = erasmusGelen;
		this.erasmusGiden = erasmusGiden;
		this.farabiGelen = farabiGelen;
		this.farabiGiden = farabiGiden;
		this.mevlanaGelen = mevlanaGelen;
		this.mevlanaGiden = mevlanaGiden;
	}
	
	public int getExchangedStudent() {
		return exchangedStudent;
	}
	public int getErasmusGelen() {
		return erasmusGelen;
	}
	public int getErasmusGiden() {
		return erasmusGiden;
	}
	public int getFarabiGelen() {
		return farabiGelen;
	}
	public int getFarabiGiden() {
		return farabiGiden;
	}
	public int getMevlanaGelen() {
		return mevlanaGelen;
	}
	public int getMevlanaGiden() {
		return mevlanaGiden;
	}
	
	public void superExchangeStudent(int exchangedStudent, int erasmusGelen, int erasmusGiden, int farabiGelen, int farabiGiden,
			int mevlanaGelen, int mevlanaGiden) {
		if(this.exchangedStudent<exchangedStudent)
		this.exchangedStudent = exchangedStudent;
		if(this.erasmusGelen<erasmusGelen)
		this.erasmusGelen = erasmusGelen;
		if(this.erasmusGiden<erasmusGiden)
		this.erasmusGiden = erasmusGiden;
		if(this.farabiGelen<farabiGelen)
		this.farabiGelen = farabiGelen;
		if(this.farabiGiden<farabiGiden)
		this.farabiGiden = farabiGiden;
		if(this.mevlanaGelen<mevlanaGelen)
		this.mevlanaGelen = mevlanaGelen;
		if(this.mevlanaGiden<mevlanaGiden)
		this.mevlanaGiden = mevlanaGiden;
	}
	
	public void worstExchangeStudent(int exchangedStudent, int erasmusGelen, int erasmusGiden, int farabiGelen, int farabiGiden,
			int mevlanaGelen, int mevlanaGiden) {
		if(this.exchangedStudent>exchangedStudent && exchangedStudent!=0)
		this.exchangedStudent = exchangedStudent;
		if(this.erasmusGelen>erasmusGelen && erasmusGelen!=0)
		this.erasmusGelen = erasmusGelen;
		if(this.erasmusGiden>erasmusGiden && erasmusGiden!=0)
		this.erasmusGiden = erasmusGiden;
		if(this.farabiGelen>farabiGelen && farabiGelen!=0)
		this.farabiGelen = farabiGelen;
		if(this.farabiGiden>farabiGiden && farabiGiden!=0)
		this.farabiGiden = farabiGiden;
		if(this.mevlanaGelen>mevlanaGelen && mevlanaGelen!=0)
		this.mevlanaGelen = mevlanaGelen;
		if(this.mevlanaGiden>mevlanaGiden && mevlanaGiden!=0)
		this.mevlanaGiden = mevlanaGiden;
	}

	public void checkWorstCases(ExchangeStudent exchangeStudents) {
		// This will be implemented if we need fill the empty values.
	}

	@Override
	public String toString() {
		return "ExchangeStudent [exchangedStudent=" + exchangedStudent + ", erasmusGelen=" + erasmusGelen
				+ ", erasmusGiden=" + erasmusGiden + ", farabiGelen=" + farabiGelen + ", farabiGiden=" + farabiGiden
				+ ", mevlanaGelen=" + mevlanaGelen + ", mevlanaGiden=" + mevlanaGiden + "]";
	}
	
}
