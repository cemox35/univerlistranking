/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */
import java.text.DecimalFormat;
 
public class University {
	protected String name;
	protected int id;
	protected UrapPoints urapScores;
	protected double entrepreneurship;
	protected int prof;
	protected int docent;
	protected int docentYard;
	protected int instructor;
	protected int foreignStudent;
	protected int foreignInstructor;
	protected int disabledStudent;
	protected ExchangeStudent exchangeStudents;
	protected BoomSocial boomsocial;
	protected StudentNumbers students;
	
	protected int commentScore=0;
	protected int favorites=0;
	protected int campusOpportunities=0;
	protected int technopark=0;
	protected int incubationCenter=0;
	protected double placementRatio=0;
	
	protected double profStudentRatio=0;
	protected double docentStudentRatio=0;
	protected double docentYardStudentRatio=0;
	protected double instructorStudentRatio=0;
	protected double foreignInstructorRatio=0;
	protected double foreignStudentRatio=0;
	protected double disabledStudentRatio=0;
	
	protected double facebookFollowerRatio=0;
	protected double twitterFollowerRatio=0;
	protected double instagramFollowerRatio=0;
	
	protected double erasmusOutcomeRatio=0;
	protected double otherExchangeRatio=0;
	
	protected double avgScore=0;
	protected double imdbScore=0;
	protected double uLScore=0;
	
	protected double erasmus_multiple=0;
	public University(){
            
        }
	public University(String name, int id, UrapPoints urapScores, double entrepreneurship, int prof, int docent,
			int docentYard, int instructor, int foreignStudent, int foreignInstructor, int disabledStudent,
			ExchangeStudent exchangeStudents, BoomSocial boomsocial, StudentNumbers students) {
		this.name = name;
		this.id = id;
		this.urapScores = urapScores;
		this.entrepreneurship = entrepreneurship;
		this.prof = prof;
		this.docent = docent;
		this.docentYard = docentYard;
		this.instructor = instructor;
		this.foreignStudent = foreignStudent;
		this.foreignInstructor = foreignInstructor;
		this.disabledStudent = disabledStudent;
		this.exchangeStudents = exchangeStudents;
		this.boomsocial = boomsocial;
		this.students = students;
	}
	
	public void checkWorstCases(WorstUniversity worstUni){
		urapScores.checkWorstCases(worstUni.getUrapScores());
		exchangeStudents.checkWorstCases(worstUni.getExchangeStudents());
		students.checkWorstCases(worstUni.getStudents());
		boomsocial.checkWorstCases(worstUni.getBoomsocial());
		if(entrepreneurship==0)
			entrepreneurship=worstUni.getEntrepreneurship() /2;
		if(prof == 0)
			prof = worstUni.getProf() /2;
		
		if(docent == 0)
			docent = worstUni.getDocent() /2;
		
		if(docentYard == 0)
			docentYard = worstUni.getDocentYard() /2;
		
		if(instructor == 0)
			instructor = worstUni.getInstructor() /2;
		
		if(foreignStudent == 0)
			foreignStudent = worstUni.getForeignStudent() /2;
		
		if(foreignInstructor == 0)
			foreignInstructor = worstUni.getForeignInstructor() /2;
		
		if(disabledStudent == 0)
			disabledStudent = worstUni.getDisabledStudent() /2;
		
		if(placementRatio == 0)
			placementRatio = worstUni.getPlacementRatio() /2;

	}
	
	public University(String name,int id){
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public UrapPoints getUrapScores() {
		return urapScores;
	}

	public double getEntrepreneurship() {
		return entrepreneurship;
	}

	public int getProf() {
		return prof;
	}

	public int getDocent() {
		return docent;
	}

	public int getDocentYard() {
		return docentYard;
	}

	public int getInstructor() {
		return instructor;
	}

	public int getForeignStudent() {
		return foreignStudent;
	}

	public int getForeignInstructor() {
		return foreignInstructor;
	}

	public int getDisabledStudent() {
		return disabledStudent;
	}

	public ExchangeStudent getExchangeStudents() {
		return exchangeStudents;
	}

	public BoomSocial getBoomsocial() {
		return boomsocial;
	}

	public StudentNumbers getStudents() {
		return students;
	}

	public void setUrapScores(UrapPoints urapScores) {
		this.urapScores = urapScores;
	}

	public void setEntrepreneurship(double entrepreneurship) {
		this.entrepreneurship = entrepreneurship;
	}

	public void setProf(int prof) {
		this.prof = prof;
	}

	public void setDocent(int docent) {
		this.docent = docent;
	}

	public void setDocentYard(int docentYard) {
		this.docentYard = docentYard;
	}

	public void setInstructor(int instructor) {
		this.instructor = instructor;
	}

	public void setForeignStudent(int foreignStudent) {
		this.foreignStudent = foreignStudent;
	}

	public void setForeignInstructor(int foreignInstructor) {
		this.foreignInstructor = foreignInstructor;
	}

	public void setDisabledStudent(int disabledStudent) {
		this.disabledStudent = disabledStudent;
	}

	public void setExchangeStudents(ExchangeStudent exchangeStudents) {
		this.exchangeStudents = exchangeStudents;
	}

	public void setBoomsocial(BoomSocial boomsocial) {
		this.boomsocial = boomsocial;
	}

	public void setStudents(StudentNumbers students) {
		this.students = students;
	}

	public void setFeatures(int features) {
		campusOpportunities=features;
	}

	public void setTechnopark(int i) {
		technopark=i;
	}

	public void setIncubationCenter(int i) {
		incubationCenter=1;
	}

	public void setFavorite(int int1) {
		favorites=int1;
	}

	public void setCommentScore(int int1) {
		commentScore=int1;
	}

	public void setPlacementRatio(double double1) {
		placementRatio=double1;
	}
	
	
	
	public double getProfStudentRatio() {
		return profStudentRatio;
	}

	public void setProfStudentRatio(double profStudentRatio) {
		this.profStudentRatio = profStudentRatio;
	}

	public double getDocentStudentRatio() {
		return docentStudentRatio;
	}

	public void setDocentStudentRatio(double docentStudentRatio) {
		this.docentStudentRatio = docentStudentRatio;
	}

	public double getDocentYardStudentRatio() {
		return docentYardStudentRatio;
	}

	public void setDocentYardStudentRatio(double docentYardStudentRatio) {
		this.docentYardStudentRatio = docentYardStudentRatio;
	}

	public double getInstructorStudentRatio() {
		return instructorStudentRatio;
	}

	public void setInstructorStudentRatio(double instructorStudentRatio) {
		this.instructorStudentRatio = instructorStudentRatio;
	}

	public double getForeignInstructorRatio() {
		return foreignInstructorRatio;
	}

	public void setForeignInstructorRatio(double foreignInstructorRatio) {
		this.foreignInstructorRatio = foreignInstructorRatio;
	}

	public double getForeignStudentRatio() {
		return foreignStudentRatio;
	}

	public void setForeignStudentRatio(double foreignStudentRatio) {
		this.foreignStudentRatio = foreignStudentRatio;
	}

	public double getDisabledStudentRatio() {
		return disabledStudentRatio;
	}

	public void setDisabledStudentRatio(double disabledStudentRatio) {
		this.disabledStudentRatio = disabledStudentRatio;
	}

	public double getPlacementRatio() {
		return placementRatio;
	}
	
	public int getIncubationCenter(){
		return incubationCenter;
	}
	
	public int getTechnopark(){
		return technopark;
	}
	
	public int getCampusOpportunities(){
		return campusOpportunities;
	}
	
	public int getFavorites(){
		return favorites;
	}
	
	public int getCommentScore(){
		return commentScore;
	}
	

	public double getFacebookFollowerRatio() {
		return facebookFollowerRatio;
	}

	public void setFacebookFollowerRatio(double facebookFollowerRatio) {
		this.facebookFollowerRatio = facebookFollowerRatio;
	}

	public double getTwitterFollowerRatio() {
		return twitterFollowerRatio;
	}

	public void setTwitterFollowerRatio(double twitterFollowerRatio) {
		this.twitterFollowerRatio = twitterFollowerRatio;
	}

	public double getInstagramFollowerRatio() {
		return instagramFollowerRatio;
	}

	public void setInstagramFollowerRatio(double instagramFollowerRatio) {
		this.instagramFollowerRatio = instagramFollowerRatio;
	}

	public double getErasmusOutcomeRatio() {
		return erasmusOutcomeRatio;
	}

	public void setErasmusOutcomeRatio(double erasmusOutcomeRatio) {
		this.erasmusOutcomeRatio = erasmusOutcomeRatio;
	}

	public double getOtherExchangeRatio() {
		return otherExchangeRatio;
	}

	public void setOtherExchangeRatio(double otherExchangeRatio) {
		this.otherExchangeRatio = otherExchangeRatio;
	}
	
	public double getAvgScore(){
		return avgScore;
	}

	public void calculateRatios(){
		profStudentRatio=(double)prof/(double)students.getTotalStudents();
		docentStudentRatio=(double)docent/(double)students.getTotalStudents();
		docentYardStudentRatio=(double)docentYard/(double)students.getTotalStudents();
		instructorStudentRatio=(double)instructor/(double)students.getTotalStudents();
		foreignInstructorRatio=(double)foreignInstructor/(double)instructor;
		foreignStudentRatio=(double)foreignStudent/(double)students.getTotalStudents();
		disabledStudentRatio=(double)disabledStudent/(double)students.getTotalStudents();
		
		/*profStudentRatio=(double)prof;
		docentStudentRatio=(double)docent;
		docentYardStudentRatio=(double)docentYard;
		instructorStudentRatio=(double)instructor;
		foreignInstructorRatio=(double)foreignInstructor;
		foreignStudentRatio=(double)foreignStudent;
		disabledStudentRatio=(double)disabledStudent;
		
		facebookFollowerRatio=(double)boomsocial.getBoomsocialFacebookFollower();
		twitterFollowerRatio=(double)boomsocial.getBoomsocialTwitterFollower();
		instagramFollowerRatio=(double)boomsocial.getBoomsocialInstagramFollower();*/
		
		erasmusOutcomeRatio=(double)exchangeStudents.getErasmusGiden()/(double)students.getTotalStudents();
		otherExchangeRatio=(double)((double)exchangeStudents.getFarabiGiden()+(double)exchangeStudents.getMevlanaGiden())/(double)students.getTotalStudents();
		
		
		
		/*erasmusOutcomeRatio=(double)exchangeStudents.getErasmusGiden();
		otherExchangeRatio=(double)((double)exchangeStudents.getFarabiGiden()+(double)exchangeStudents.getMevlanaGiden());*/
	}
	
	
	public void calculateAverageScore(SuperUniversity superUni,double ahpOgretim,double ahpArastirma,double ahpAltyapi,double ahpSosyal,double ahpTercih,double ahpUluslararasi){
		double urapDoctorate = this.urapScores.getUrapDoctorate() / superUni.getUrapScores().getUrapDoctorate();
		//System.out.println(name+" "+this.urapScores.getUrapDoctorate()+" "+superUni.getUrapScores().getUrapDoctorate());
		
		double urapInstructor = this.urapScores.getUrapInstructor() / superUni.getUrapScores().getUrapInstructor();
		//System.out.println(name+" "+this.urapScores.getUrapInstructor()+" "+superUni.getUrapScores().getUrapInstructor());
		
		double urapArticle = this.urapScores.getUrapArticle() / superUni.getUrapScores().getUrapArticle();
		//System.out.println(name+" "+this.urapScores.getUrapArticle()+" "+superUni.getUrapScores().getUrapArticle());
		
		double urapCitation = this.urapScores.getUrapCitation() / superUni.getUrapScores().getUrapCitation();
		//System.out.println(name+" "+this.urapScores.getUrapCitation()+" "+superUni.getUrapScores().getUrapCitation());
		
		double urapDocument = this.urapScores.getUrapDocument() / superUni.getUrapScores().getUrapDocument();
		//System.out.println(name+" "+this.urapScores.getUrapDocument()+" "+superUni.getUrapScores().getUrapDocument());

		
		double foreignStu = foreignStudentRatio / superUni.getForeignStudentRatio();
		/*double profStuRatio = (double)profStudentRatio/(double)superUni.getProfStudentRatio();
		double docentStuRatio =(double) docentStudentRatio/(double)superUni.getDocentStudentRatio();
		double docentYardStuRatio = (double)docentYardStudentRatio/(double)superUni.getDocentYardStudentRatio();
		double insStuRatio = (double)instructorStudentRatio/(double)superUni.getInstructorStudentRatio();
		double foreignStuRatio =(double) foreignStudentRatio/(double)superUni.getForeignStudentRatio();
		double foreignInsRatio = (double)foreignInstructorRatio/(double)superUni.getForeignInstructorRatio();
		double disabledStuRatio = (double)disabledStudentRatio /(double)superUni.getDisabledStudentRatio();*/
		
		double girisimcilik = entrepreneurship / superUni.getEntrepreneurship();
		int kulucka = incubationCenter/superUni.getIncubationCenter();
		int teknopark = technopark / superUni.getTechnopark();
		double olanaklar =(double) campusOpportunities /(double) superUni.getCampusOpportunities();
		
		double twitterTakipci = (double)boomsocial.getBoomsocialTwitterFollower() / (double)superUni.getBoomsocial().getBoomsocialTwitterFollower();
		double facebookTakipci = (double)boomsocial.getBoomsocialFacebookFollower() / (double)superUni.getBoomsocial().getBoomsocialFacebookFollower();
		double instagramTakipci = (double)boomsocial.getBoomsocialInstagramFollower() / (double)superUni.getBoomsocial().getBoomsocialInstagramFollower();
		
		double twitterScore = (double)boomsocial.getBoomsocialTwitterScore() / (double) superUni.getBoomsocial().getBoomsocialTwitterScore();
		double instagramScore = (double)boomsocial.getBoomsocialInstagramScore() / (double) superUni.getBoomsocial().getBoomsocialInstagramScore();
		double facebookScore = (double)boomsocial.getBoomsocialFacebookScore() / (double) superUni.getBoomsocial().getBoomsocialFacebookScore();
		/*double favori =(double) favorites /(double) superUni.getFavorites();
		double puan = (double)commentScore / (double)superUni.getCommentScore();*/
		if(placementRatio<30)
			placementRatio = 50;
		double yerlestirme = placementRatio / superUni.getPlacementRatio();
		double multipleErasmusRatio = (double)erasmus_multiple / (double)superUni.getErasmus_multiple();
		/*double erasmusGidenOran = erasmusOutcomeRatio / superUni.getErasmusOutcomeRatio();
		double otherExchangeOran = otherExchangeRatio / superUni.getOtherExchangeRatio();*/
		
		/*double ogretim = (double)(urapDoctorate+urapInstructor+profStuRatio+docentStuRatio+docentYardStuRatio+insStuRatio+foreignStuRatio+foreignInsRatio+disabledStuRatio) * ahpOgretim;
		double arastirma = (urapArticle+urapCitation+urapDocument+girisimcilik)*ahpArastirma;
		double altyapi = (disabledStuRatio+kulucka+teknopark+olanaklar)*ahpAltyapi;
		double sosyal = (olanaklar+twitterTakipci+facebookTakipci+instagramTakipci)*ahpSosyal;
		double tercih = (favori+puan+yerlestirme)*ahpTercih;
		double uluslararasi = (foreignStuRatio+foreignInsRatio+erasmusGidenOran+otherExchangeOran)*ahpUluslararasi;*/
		
		double ogretim = (double)(urapDoctorate+urapInstructor) * ahpOgretim;
		double arastirma = (urapArticle+urapCitation+urapDocument+girisimcilik)*ahpArastirma;
		double altyapi = (kulucka+teknopark+olanaklar)*ahpAltyapi;
		double sosyal = ((twitterScore+instagramScore+facebookScore)/3)*ahpSosyal;
		double tercih = (yerlestirme)*ahpTercih;
		//double uluslararasi = (foreignStu+multipleErasmusRatio)*ahpUluslararasi;
		//System.out.println("ULUSLARARASI RESULT = "+uluslararasi);
		
		/*if (name.contains("EKONOMİ") || name.contains("YAŞAR") || name.contains("EGE") || name.contains("MARMARA")) {
			System.out.println(name+" "+foreignStu+" "+ogretim+" "+arastirma+" "+altyapi+" "+sosyal+" "+tercih+" "+uluslararasi);
			System.out.println(" Uraps = "+urapArticle+" "+urapCitation+" "+urapDoctorate+" "+urapDocument+" "+urapInstructor);
			System.out.println();
		}*/
		avgScore = (double)(ogretim+arastirma+altyapi+sosyal+tercih)/(double)100;
			/*System.out.println("NORMAL SUM = UrapDoctorate = "+urapDoctorate+" UrapInstructor"+urapInstructor+" urapArticle "+urapArticle+" urapCitation "+urapCitation+" UrapDocument "+urapDocument+" girisimcilik "+girisimcilik
					+" kulucka "+kulucka+" teknopark "+teknopark+" olanaklar "+olanaklar+" twitterScore "+twitterScore+" instagramScore "+instagramScore+" facebookScore "+facebookScore
					+" yerlestirme "+yerlestirme+" foreignStu "+foreignStu+" multipleErasmusRatio "+multipleErasmusRatio);
			System.out.println(" ");
			System.out.println(name+" WEIGHTED SUMS");
			System.out.println("ogretim = "+ogretim+" arastirma="+arastirma+" altyapi = "+altyapi+" sosyal = "+sosyal+" tercih = "+tercih+" uluslararasi = "+uluslararasi);
		*/
		/*System.out.println(name+" urapDoctore"+urapDoctorate+" urapInstructor = "+urapInstructor+" urapArticle = "+urapArticle+" urapCitation = "+urapCitation+" urapDocument = "+urapDocument+" girisimcilik = "+girisimcilik+" kulucka = "+kulucka+" teknopark = "+teknopark+" olanaklar = "+olanaklar+" twitterScore = "+twitterScore+" facebookScore = "+facebookScore+" instagramScore = "+instagramScore
				+" yerlestirme = "+yerlestirme+" ogretim = "+ogretim+" arastirma = "+arastirma+" altyapı = "+altyapi+" sosyal = "+sosyal+" tercih = "+tercih+" uluslaraarasi = "+uluslararasi+" avgScore = "+avgScore);
		System.out.println("  ");*/
		/*System.out.println(name+" urapDoctore"+urapDoctorate+" urapInstructor = "+urapInstructor+" urapArticle = "+urapArticle+" urapCitation = "+urapCitation+" urapDocument = "+urapDocument+" profStuRatio = "+profStuRatio+" docentStuRatio = "+docentStuRatio+" docentYardStuRadio"+docentYardStuRatio+" insStuRatio = "+
		insStuRatio+" foreignStuRatio = "+foreignStuRatio+" foreignInsRatio = "+foreignInsRatio+" disabledStuRatio = "+disabledStuRatio+" girisimcilik = "+girisimcilik+" kulucka = "+kulucka+" teknopark = "+teknopark+" olanaklar = "+olanaklar+" twitterTakipci = "+twitterTakipci+" facebookTakipci = "+facebookTakipci+" instagramTakipci = "+instagramTakipci
		+" favori = "+favori+" puan = "+puan+" yerlestirme = "+yerlestirme+" erasmusGidenOran = "+erasmusGidenOran+" otherExchangeOran = "+otherExchangeOran+" ogretim = "+ogretim+" arastirma = "+arastirma+" altyapı = "+altyapi+" sosyal = "+sosyal+" tercih = "+tercih+" uluslaraarasi = "+uluslararasi+" avgScore = "+avgScore);
	*/
	}

	public double calculateIMDBScore(double superAvg) {
		//System.out.println(" AvgScore = "+avgScore+" SuperAvg = "+superAvg);
		imdbScore=(10*avgScore)/superAvg;
		//System.out.println(name+" imdb = "+imdbScore);
		double test = imdbScore * (1.0-((imdbScore-10.0)/10.0));
		return test;
	}
	
	public double getIMDBScore(){
		return imdbScore;
	}
	
	public void imdbWithoutDeviation(double standartSapma, double stDEV, double superAVG,double minIMDB,double maxIMDB){
		double x = imdbScore * (1.0-((imdbScore-10.0)/10.0));
		double min = 4.4;
		double max = 9.4;
		double a = (maxIMDB-x)/(x-minIMDB);
		imdbScore = (max+(min*a))/(a+1);
		//System.out.println("imdbScore = "+imdbScore);
		/*if (name.contains("ORTA DOĞU")) {
			System.out.println("Bulunan min imdb = "+minIMDB);
			System.out.println("Bulunan max imdb = "+maxIMDB);
			System.out.println("x = "+x+" min = "+min+" max = "+max+" a = "+a+" imdbScore = "+(max+(min*a))/(a+1)+" = "+imdbScore);*/
		//}
		/*double avgStDev = avgScore * stDEV;
		imdbScore = (10*avgStDev)/superAVG;
		imdbScore = imdbScore * (1.0-((imdbScore-10.0)/10.0));*/
		//System.out.println(name+" Sapma ile carpilip superUni Avg bolunmus hali = "+imdbScore);
	}

	@Override
	public String toString() {
		return "University [name=" + name + ", id=" + id + ", urapScores=" + urapScores.toString() + ", entrepreneurship="
				+ entrepreneurship + ", prof=" + prof + ", docent=" + docent + ", docentYard=" + docentYard
				+ ", instructor=" + instructor + ", foreignStudent=" + foreignStudent + ", foreignInstructor="
				+ foreignInstructor + ", disabledStudent=" + disabledStudent + ", exchangeStudents=" + exchangeStudents.toString()
				+ ", boomsocial=" + boomsocial.toString() + ", students=" + students.toString() + ", commentScore=" + commentScore
				+ ", favorites=" + favorites + ", campusOpportunities=" + campusOpportunities + ", technopark="
				+ technopark + ", incubationCenter=" + incubationCenter + ", placementRatio=" + placementRatio
				+ ", profStudentRatio=" + profStudentRatio + ", docentStudentRatio=" + docentStudentRatio
				+ ", docentYardStudentRatio=" + docentYardStudentRatio + ", instructorStudentRatio="
				+ instructorStudentRatio + ", foreignInstructorRatio=" + foreignInstructorRatio
				+ ", foreignStudentRatio=" + foreignStudentRatio + ", disabledStudentRatio=" + disabledStudentRatio
				+ ", facebookFollowerRatio=" + facebookFollowerRatio + ", twitterFollowerRatio=" + twitterFollowerRatio
				+ ", instagramFollowerRatio=" + instagramFollowerRatio + ", erasmusOutcomeRatio=" + erasmusOutcomeRatio
				+ ", otherExchangeRatio=" + otherExchangeRatio + ", avgScore=" + avgScore + ", imdbScore=" + imdbScore
				+ ", uLScore=" + uLScore + "]";
	}

	public double getErasmus_multiple() {
		return erasmus_multiple;
	}

	public void setErasmus_multiple(double erasmus_multiple) {
		this.erasmus_multiple = erasmus_multiple;
	}
	
	
}

