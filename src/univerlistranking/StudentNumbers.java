/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */

public class StudentNumbers {
	int associateWoman = 0;
	int associateMan = 0;
	int bachelorWoman = 0;
	int bachelorMan = 0;
	int masterWoman = 0;
	int masterMan = 0;
	int doctorateWoman = 0;
	int doctorateMan = 0;
	int totalWoman = 0;
	int totalMan = 0;
	
	public StudentNumbers(int associateWoman, int associateMan, int bachelorWoman, int bachelorMan, int masterWoman,
			int masterMan, int doctorateWoman, int doctorateMan, int totalWoman, int totalMan) {
		this.associateWoman = associateWoman;
		this.associateMan = associateMan;
		this.bachelorWoman = bachelorWoman;
		this.bachelorMan = bachelorMan;
		this.masterWoman = masterWoman;
		this.masterMan = masterMan;
		this.doctorateWoman = doctorateWoman;
		this.doctorateMan = doctorateMan;
		this.totalWoman = totalWoman;
		this.totalMan = totalMan;
	}

	public int getAssociateWoman() {
		return associateWoman;
	}

	public int getAssociateMan() {
		return associateMan;
	}

	public int getBachelorWoman() {
		return bachelorWoman;
	}

	public int getBachelorMan() {
		return bachelorMan;
	}

	public int getMasterWoman() {
		return masterWoman;
	}

	public int getMasterMan() {
		return masterMan;
	}

	public int getDoctorateWoman() {
		return doctorateWoman;
	}

	public int getDoctorateMan() {
		return doctorateMan;
	}

	public int getTotalWoman() {
		return totalWoman;
	}

	public int getTotalMan() {
		return totalMan;
	}
	
	public void superStudentNumbers(int associateWoman, int associateMan, int bachelorWoman, int bachelorMan, int masterWoman,
			int masterMan, int doctorateWoman, int doctorateMan, int totalWoman, int totalMan) {
		if(this.associateWoman < associateWoman)
		this.associateWoman = associateWoman;
		if(this.associateMan < associateMan)
		this.associateMan = associateMan;
		if(this.bachelorWoman < bachelorWoman)
		this.bachelorWoman = bachelorWoman;
		if(this.bachelorMan < bachelorMan)
		this.bachelorMan = bachelorMan;
		if(this.masterWoman < masterWoman)
		this.masterWoman = masterWoman;
		if(this.masterMan < masterMan)
		this.masterMan = masterMan;
		if(this.doctorateWoman < doctorateWoman)
		this.doctorateWoman = doctorateWoman;
		if(this.doctorateMan < doctorateMan)
		this.doctorateMan = doctorateMan;
		if(this.totalWoman < totalWoman)
		this.totalWoman = totalWoman;
		if(this.totalMan < totalMan)
		this.totalMan = totalMan;
	}
	
	public void worstStudentNumbers(int associateWoman, int associateMan, int bachelorWoman, int bachelorMan, int masterWoman,
			int masterMan, int doctorateWoman, int doctorateMan, int totalWoman, int totalMan) {
		if(this.associateWoman < associateWoman && associateWoman!=0)
		this.associateWoman = associateWoman;
		if(this.associateMan < associateMan && associateMan!=0)
		this.associateMan = associateMan;
		if(this.bachelorWoman < bachelorWoman && bachelorWoman!=0)
		this.bachelorWoman = bachelorWoman;
		if(this.bachelorMan < bachelorMan && bachelorMan!=0)
		this.bachelorMan = bachelorMan;
		if(this.masterWoman < masterWoman && masterWoman!=0)
		this.masterWoman = masterWoman;
		if(this.masterMan < masterMan && masterMan!=0)
		this.masterMan = masterMan;
		if(this.doctorateWoman < doctorateWoman && doctorateWoman!=0)
		this.doctorateWoman = doctorateWoman;
		if(this.doctorateMan < doctorateMan && doctorateMan!=0)
		this.doctorateMan = doctorateMan;
		if(this.totalWoman < totalWoman && totalWoman!=0)
		this.totalWoman = totalWoman;
		if(this.totalMan < totalMan && totalMan!=0)
		this.totalMan = totalMan;
	}
	
	public int getTotalStudents(){
		return totalMan+totalWoman;
	}

	public void checkWorstCases(StudentNumbers students) {
		if(totalMan == 0)
			totalMan=students.getTotalMan() /2;
		if(totalWoman == 0)
			totalWoman=students.getTotalWoman() /2;
	}

	@Override
	public String toString() {
		return "StudentNumbers [associateWoman=" + associateWoman + ", associateMan=" + associateMan
				+ ", bachelorWoman=" + bachelorWoman + ", bachelorMan=" + bachelorMan + ", masterWoman=" + masterWoman
				+ ", masterMan=" + masterMan + ", doctorateWoman=" + doctorateWoman + ", doctorateMan=" + doctorateMan
				+ ", totalWoman=" + totalWoman + ", totalMan=" + totalMan + "]";
	}
	
	
}
