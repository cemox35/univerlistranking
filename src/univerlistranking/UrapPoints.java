/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */
 
public class UrapPoints {
	double urapArticle=15;
	double urapCitation=10;
	double urapDocument=15;
	double urapInstructor=30;
	double urapDoctorate=10;
	public UrapPoints(double urapArticle, double urapCitation, double urapDocument, double urapInstructor,
			double urapDoctorate) {
		this.urapArticle = urapArticle;
		this.urapCitation = urapCitation;
		this.urapDocument = urapDocument;
		this.urapInstructor = urapInstructor;
		this.urapDoctorate = urapDoctorate;
		
	}
	public double getUrapArticle() {
		return urapArticle;
	}
	public double getUrapCitation() {
		return urapCitation;
	}
	public double getUrapDocument() {
		return urapDocument;
	}
	public double getUrapInstructor() {
		return urapInstructor;
	}
	public double getUrapDoctorate() {
		return urapDoctorate;
	}
	
	public void superUrapPoints(double urapArticle, double urapCitation, double urapDocument, double urapInstructor,
			double urapDoctorate){
		if (this.urapArticle<urapArticle)
			this.urapArticle=urapArticle;
		if (this.urapCitation<urapCitation)
			this.urapCitation=urapCitation;
		if (this.urapDocument<urapDocument)
			this.urapDocument=urapDocument;
		if (this.urapInstructor<urapInstructor)
			this.urapInstructor=urapInstructor;
		if (this.urapDoctorate<urapDoctorate)
			this.urapDoctorate=urapDoctorate;
	}
	
	public void worstUrapPoints(double urapArticle, double urapCitation, double urapDocument, double urapInstructor,
			double urapDoctorate){
		if (this.urapArticle>urapArticle && urapArticle>15)
			this.urapArticle=urapArticle;
		if (this.urapCitation>urapCitation && urapCitation>10)
			this.urapCitation=urapCitation;
		if (this.urapDocument>urapDocument && urapDocument>15)
			this.urapDocument=urapDocument;
		if (this.urapInstructor>urapInstructor && urapInstructor>30)
			this.urapInstructor=urapInstructor;
		if (this.urapDoctorate>urapDoctorate && urapDoctorate>10)
			this.urapDoctorate=urapDoctorate;
	}
	public void checkWorstCases(UrapPoints urapScores) {
		// Just check one variable because if there is one initialized then others are initialized too.
		if(urapArticle==0){
				this.urapArticle=urapScores.getUrapArticle() /2;
				this.urapCitation=urapScores.getUrapCitation() /2;
				this.urapDocument=urapScores.getUrapDocument() /2;
				this.urapInstructor=urapScores.getUrapInstructor() /2;
				this.urapDoctorate=urapScores.getUrapDoctorate() /2;
		}
	}
	@Override
	public String toString() {
		return "UrapPoints [urapArticle=" + urapArticle + ", urapCitation=" + urapCitation + ", urapDocument="
				+ urapDocument + ", urapInstructor=" + urapInstructor + ", urapDoctorate=" + urapDoctorate + "]";
	}
	
	
}

