/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package univerlistranking;

/**
 *
 * @author ASUS
 */

public class SuperUniversity extends University {

	public SuperUniversity(String name, int id, UrapPoints urapScores, double entrepreneurship, int prof, int docent,
			int docentYard, int instructor, int foreignStudent, int foreignInstructor, int disabledStudent,
			ExchangeStudent exchangeStudents, BoomSocial boomsocial, StudentNumbers students) {
		super(name, id, urapScores, entrepreneurship, prof, docent, docentYard, instructor, foreignStudent, foreignInstructor,
				disabledStudent, exchangeStudents, boomsocial, students);
		// TODO Auto-generated constructor stub
	}
	
	public SuperUniversity(String name, int id) {
		super(name, id);
		// TODO Auto-generated constructor stub
	}
	
	public SuperUniversity() {
		super("Super University",0,new UrapPoints(0, 0, 0, 0, 0),0,0,0,0,0,0,0,0,new ExchangeStudent(0, 0, 0, 0, 0, 0, 0),new BoomSocial(0, 0, 0, 0, 0, 0, 0, 0, 0),new StudentNumbers(0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
		technopark=1;
		incubationCenter=1;
	}


	public void superBoomsocial(int int1, int int2, int int3, int int4, int int5, int int6, int int7, int int8,
			int int9) {
		boomsocial.superBoomSocial(int1, int2, int3, int4, int5, int6, int7, int8, int9);
	}

	public void superDisabledStudent(int int1) {
		if(disabledStudent < int1)
			disabledStudent=int1;
	}

	public void superDocent(int int1) {
		if(docent < int1)
			docent=int1;
	}

	public void superDocentYard(int int1) {
		if(docentYard < int1)
			docentYard=int1;
	}

	public void superEntrepreneurship(double double1) {
		if(entrepreneurship < double1)
			entrepreneurship=double1;
	}

	public void superForeignInstructor(int int1) {
		if(foreignInstructor < int1)
			foreignInstructor=int1;
		
	}

	public void superForeignStudent(int int1) {
		if(foreignStudent < int1)
			foreignStudent=int1;
		
	}

	public void superInstructor(int int1) {
		if(instructor < int1)
			instructor=int1;
		
	}

	public void superProf(int int1) {
		if(prof < int1)
			prof=int1;
		
	}
	
	public void superExchangeStudents(int int1, int int2, int int3, int int4, int int5, int int6, int int7) {
		exchangeStudents.superExchangeStudent(int1, int2, int3, int4, int5, int6, int7);
	}
	
	public void superStudents(int int1, int int2, int int3, int int4, int int5, int int6, int int7, int int8, int int9,
			int int10) {
		students.superStudentNumbers(int1, int2, int3, int4, int5, int6, int7, int8, int9, int10);
	}

	public void superUrapScores(double double1, double double2, double double3, double double4, double double5) {
		urapScores.superUrapPoints(double1, double2, double3, double4, double5);
	}

	public void superFeatures(int int1) {
		if (campusOpportunities < int1) {
			campusOpportunities=int1;
		}
	}

	public void superFavorite(int int1) {
		if (favorites<int1) {
			favorites=int1;
		}
	}

	public void superCommentScore(int int1) {
		if (commentScore < int1) {
			commentScore = int1;
		}
	}

	public void superPlacementRatio(double double1) {
		if (placementRatio < double1) {
			placementRatio=double1;
		}
		
	}

	public void superRatios(double profStudentRatio, double docentStudentRatio, double docentYardStudentRatio,
			double instructorStudentRatio, double foreignStudentRatio, double foreignInstructorRatio,
			double disabledStudentRatio, double d, double e, double f, double g, double h) {
		if (this.profStudentRatio < profStudentRatio)
			this.profStudentRatio = profStudentRatio;
		
		if (this.docentStudentRatio < docentStudentRatio)
			this.docentStudentRatio = docentStudentRatio;
		
		if (this.docentYardStudentRatio < docentYardStudentRatio)
			this.docentYardStudentRatio = docentYardStudentRatio;
		
		if (this.instructorStudentRatio < instructorStudentRatio)
			this.instructorStudentRatio = instructorStudentRatio;
		
		if (this.foreignStudentRatio < foreignStudentRatio)
			this.foreignStudentRatio = foreignStudentRatio;
		
		if (this.foreignInstructorRatio < foreignInstructorRatio)
			this.foreignInstructorRatio = foreignInstructorRatio;
		
		if (this.disabledStudentRatio < disabledStudentRatio)
			this.disabledStudentRatio = disabledStudentRatio;
		
		if(facebookFollowerRatio < d)
			facebookFollowerRatio = d;
		
		if(twitterFollowerRatio < e)
			twitterFollowerRatio = e;
		
		if(instagramFollowerRatio < f)
			instagramFollowerRatio = f;
		
		if(erasmusOutcomeRatio < g)
			erasmusOutcomeRatio = g;
		
		if(otherExchangeRatio < g)
			otherExchangeRatio = g;
		
	}
	
	public void superErasmus_multiple(int int1) {
		if (erasmus_multiple < int1) {
			erasmus_multiple = int1;
		}
	}

}

